from flask import Flask, request, jsonify
from flask import render_template

from get_air_q import *
from get_weather import *

app = Flask(__name__)


@app.route("/")
def main():
    return render_template('index.html')


@app.route("/api/update_air_q")
def update_air_q():
    info = get_air_q()
    return jsonify(info)


@app.route("/api/update_weather")
def update_weather():
    info = get_weather()
    return jsonify(info)


if __name__ == "__main__":
    app.run()
