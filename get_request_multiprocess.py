import requests
import concurrent.futures
import time

urls = ["http://faker.hook.io", "http://faker.hook.io", "http://faker.hook.io", "http://faker.hook.io"]

t1 = time.perf_counter()


def run_req(url):
    with requests.get(url) as response:
        return response.json()


if __name__ == "__main__":
    # Sync
    # for i in urls:
    #     print(run_req(i))

    # Multiprocess
    # with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
    #     executor.map(run_req, urls)

    # Miltithread
    # with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    #     future_to_url = {executor.submit(run_req, url): url for url in urls}
    #     for future in concurrent.futures.as_completed(future_to_url):
    #         data = future.result()
    #         print(data)

    t2 = time.perf_counter()

    print(f'finished in {t2 - t1} seconds')
