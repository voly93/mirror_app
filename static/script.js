moment.locale('pl');

function update_air_q(){
    $.ajax({
    url: '/api/update_air_q',
    dataType: 'json',
    })
    .done(function(data) {
        $("#airQuality").text(data['pm10IndexLevel']['indexLevelName']);}
    )
    .fail(function() {
        console.log("Ajax failed to fetch air_q")
    })};

function update_weather(){
    $.ajax({
    url: '/api/update_weather',
    dataType: 'json',
    })
    .done(function(data) {
        let current_weather = data['current_weather']['main']['temp'];
        current_weather = Math.round(current_weather,1);
        $("#currentTemp").text(current_weather);

        let feels_like = data['current_weather']['main']['feels_like'];
        feels_like = Math.round(feels_like,1);
        $("#feelsLike").text(feels_like);

        $("#currentWeather").text(data['current_weather']['weather'][0]['description']);

        //future dates weather
        let day1 = moment().add(1, 'days');
        let day2 = moment().add(2, 'days');
        let day3 = moment().add(3, 'days');
        let day4 = moment().add(4, 'days');
        let day5 = moment().add(5, 'days');

        $("#day1MinTemp").text(data['future_weather'][day1.format("YYYY-MM-DD")][0]['temp_min']);
        $("#day2MinTemp").text(data['future_weather'][day2.format("YYYY-MM-DD")][0]['temp_min']);
        $("#day3MinTemp").text(data['future_weather'][day3.format("YYYY-MM-DD")][0]['temp_min']);
        $("#day4MinTemp").text(data['future_weather'][day4.format("YYYY-MM-DD")][0]['temp_min']);
        $("#day5MinTemp").text(data['future_weather'][day5.format("YYYY-MM-DD")][0]['temp_min']);

        $("#day1MaxTemp").text(data['future_weather'][day1.format("YYYY-MM-DD")][0]['temp_max']);
        $("#day2MaxTemp").text(data['future_weather'][day2.format("YYYY-MM-DD")][0]['temp_max']);
        $("#day3MaxTemp").text(data['future_weather'][day3.format("YYYY-MM-DD")][0]['temp_max']);
        $("#day4MaxTemp").text(data['future_weather'][day4.format("YYYY-MM-DD")][0]['temp_max']);
        $("#day5MaxTemp").text(data['future_weather'][day5.format("YYYY-MM-DD")][0]['temp_max']);

        $("#day1RainChance").text(data['future_weather'][day1.format("YYYY-MM-DD")][0]['rain_chance']);
        $("#day2RainChance").text(data['future_weather'][day2.format("YYYY-MM-DD")][0]['rain_chance']);
        $("#day3RainChance").text(data['future_weather'][day3.format("YYYY-MM-DD")][0]['rain_chance']);
        $("#day4RainChance").text(data['future_weather'][day4.format("YYYY-MM-DD")][0]['rain_chance']);
        $("#day5RainChance").text(data['future_weather'][day5.format("YYYY-MM-DD")][0]['rain_chance']);

        $("#day1WeatherMain").text(data['future_weather'][day1.format("YYYY-MM-DD")][0]['weather_sum']);
        $("#day2WeatherMain").text(data['future_weather'][day2.format("YYYY-MM-DD")][0]['weather_sum']);
        $("#day3WeatherMain").text(data['future_weather'][day3.format("YYYY-MM-DD")][0]['weather_sum']);
        $("#day4WeatherMain").text(data['future_weather'][day4.format("YYYY-MM-DD")][0]['weather_sum']);
        $("#day5WeatherMain").text(data['future_weather'][day5.format("YYYY-MM-DD")][0]['weather_sum']);

        $("#day1WeatherIcon").addClass(data['future_weather'][day1.format("YYYY-MM-DD")][0]['weather_main_sum']);
        $("#day2WeatherIcon").addClass(data['future_weather'][day2.format("YYYY-MM-DD")][0]['weather_main_sum']);
        $("#day3WeatherIcon").addClass(data['future_weather'][day3.format("YYYY-MM-DD")][0]['weather_main_sum']);
        $("#day4WeatherIcon").addClass(data['future_weather'][day4.format("YYYY-MM-DD")][0]['weather_main_sum']);
        $("#day5WeatherIcon").addClass(data['future_weather'][day5.format("YYYY-MM-DD")][0]['weather_main_sum']);

        if ( $( ".weatherIcon" ).is( ".Snow" ) ) {

            $(".weatherIcon").addClass("wi wi-snow");

}

        console.log(day1)}
    )
    .fail(function() {
        console.log("Ajax failed to fetch weather")
    })};

function getTime(){
    var now = moment();
    $("#time").text(now.format("HH:mm"));
    $("#date").text(now.format("dddd, D MMM. "));

    let day1 = moment().add(1, 'days');
    let day2 = moment().add(2, 'days');
    let day3 = moment().add(3, 'days');
    let day4 = moment().add(4, 'days');
    let day5 = moment().add(5, 'days');

    $("#day1Date").text(day1.format("ddd, D"));
    $("#day2Date").text(day2.format("ddd, D"));
    $("#day3Date").text(day3.format("ddd, D"));
    $("#day4Date").text(day4.format("ddd, D"));
    $("#day5Date").text(day5.format("ddd, D"));


}

$(document).ready(function(){
    update_air_q();
    update_weather();

    setInterval(update_air_q,50000);
    setInterval(update_weather,50000);
    setInterval(getTime,1000);
});


