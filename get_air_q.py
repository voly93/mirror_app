import requests


def get_air_q():
    url = "http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/401"
    r = requests.get(url)
    return r.json()


if __name__ == "__main__":
    print(get_air_q())
