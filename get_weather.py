import requests
import json
from collections import Counter


def get_current():
    url = "http://api.openweathermap.org/data/2.5/weather?id=3094802&appid=163248a04e6dd4f214df76f047c32e74&units=metric&lang=pl"
    r = requests.get(url)
    return r.json()


def get_furure():
    url = "http://api.openweathermap.org/data/2.5/forecast?id=3094802&appid=163248a04e6dd4f214df76f047c32e74&units=metric&lang=pl"
    r = requests.get(url)
    return r.json()


def parse_future(data):
    data = get_furure()["list"]
    # main key is date
    days_temp = {}
    for i in data:
        for k, v in i.items():
            if k == "dt_txt":
                # get rid of the time from the date
                p_day = i["dt_txt"].split()
                p_day = p_day[0]

                # add k, v to existing entries
                if p_day in days_temp.keys():
                    days_temp[p_day][0]["temp"].append(i["main"]["temp"])
                    days_temp[p_day][0]["weather"].append(i["weather"][0]["description"])
                    days_temp[p_day][0]["weather_main"].append(i["weather"][0]["main"])
                # create k, v for non-existing entries
                else:
                    days_temp[p_day] = [{"temp": [i["main"]["temp"]],
                                         "weather": [i["weather"][0]["description"]],
                                         "weather_main": [i["weather"][0]["main"]]}]

    for k, v in days_temp.items():
        # create min, max temps from average and add to dict
        temp_min = round(min(v[0]["temp"]))
        temp_max = round(max(v[0]["temp"]))
        days_temp[k][0]["temp_min"] = temp_min
        days_temp[k][0]["temp_max"] = temp_max
        # count occurences for weather and weather_main for icon and add to dict most common value
        weather_sum = Counter(v[0]['weather'])
        days_temp[k][0]["weather_sum"] = weather_sum.most_common()[0][0]
        weather_main_sum = Counter(v[0]['weather_main'])
        days_temp[k][0]["weather_main_sum"] = weather_main_sum.most_common()[0][0]

        count_weather = 1
        if len(v[0]['weather_main']) == 0:
            pass
        else:
            count_weather = len(v[0]['weather_main'])

        rain_chance = weather_main_sum['Rain'] / count_weather * 100
        days_temp[k][0]["rain_chance"] = round(rain_chance)

    days = days_temp

    return days


# combine current and future weather to a single json
def get_weather():
    data = {"current_weather": get_current(), "future_weather": parse_future(get_furure())}

    return data


if __name__ == "__main__":
    print(get_weather())
